# skyKill
Are you running Skyrim or Fallout 4 on linux? Finding it impossible to quit to desktop? Do you like Wallabys? If you answered yes to any of these questions, this product is for you. Now you can kill all those pesky windows games (Skyrim, Fallout 4) running on your linux desktop with a simple click. That's right. You read me. So easy even a Wallaby can do it. (No Wallabys were harmed in the writing of this description.) Created by But-Ass-Cold Coding House, Wanka Wanka, Tetontown

This is my learn GIT project. It's not particularly useful beyond a demonstration of how to use pyinstaller. I chose to make skyKill my learn git project because I found myself unable to kill skyrim running steam proton on Linux. I remember it working fine before I went on my modding rampage. So I would have to pop up a terminal and write.
```Python
import psutil as ps
[x.kill() for x in ps.process_iter() if '.exe' in x.name()]
```
I found that this was the fastest way I could kill all the processes that steam proton sets up to run its windows emulation.

Well I have been playing skyrim for about 8 months and it sucks to have to type it in to the console every time. I also really needed a small project to learn git on.

I just installed Fallout 4 and it has problems quiting if you choose the "Quit to Desktop" option so I added it to this program.

This program will look for other Wine processes that may be running and will leave them running when it kills the Skyrim or Fallout 4. Handy if you are running Vortex Mod Manager in Wine. It will still be running after you use skyKill. If no other Wine applications are running skyKill will ALL processes running (anything with a .exe extension).

Reasons to not use my program. 
1. You don't need to. Far enough. You kind of win that argument.
2. You are running windows. skyKill works in windows with a windows compile as proof of x-compatibility code. Unnecessary though as I have never had a problem shutting down skyrim on windows. If you are lazy and have two monitors this might be useful.
3. Your running OSX. I don't even know if steam supports OSX or skyrim can be emulated in OSX. It is not beyond the scope of this project though. I have thought I might use this as a bases for a video series on coding once for multiple OS and some problems I have found answers to.
4. Your running linux. What? Well, there really isn't much difference in my code that it couldn't be adapted to run in python 2.7 or 3.6, remove 2 of these "(" and two if those ")" and remove one number from the shebang and bobs your uncle. You can literally place the .py file anywhere and use a .desktop file to point to any file and/or icon you want as far as the installer is concerned. One more drawback is the compiled file's file size. With the compile its huge. The skyKill executable is 5.7MB. The skyKill.py file is 2.02KB. Of course it would be, it has to bring all of python with it. So it not very practical as anything other that a working novelty git learning/possibly teaching project that has a marginally practical purpose. Just not a good one.

Reasons I'll likely use the application.
1. I'm really lazy.
2. Because I can.

So you're a rebel huh? Wanna give her a whirl? Compile it your damn self. Just kidding. Just remember that you are doing this at your own risk. This program comes without warranty. 100% of one Wallaby surveyed reported no ill effects and he's been at this for a few days now. But still.

### Windows Install:
    Needs to be Windows 7 or greater 64 bit only.
1. Download the file from [here][1] and run from anywhere. It is a stand alone application. No install required. I might build an installer later as a demo.

[1]: https://github.com/tetoNidan/skyKill/blob/master/dist/skyKill.exe

### Linux Install:
    Like all good linux apps there are a few ways to do so depending on what you want. 
    Justs needs 16.04 or better. Probably will work on debain equivalents.

#### Use my installer:
```**Note:** I fixed root install. This is only useful when skyrim wont release your mouse from the game. This happens to me all the time playing Skyrim but I dont have enough play time in Fallout to know if this game is affected. If you don't have that problem root install is pointless. Root install installs the skyKill program in your /usr/local/sbin directory. If you have root install you can just open a terminal and type skykill if you chose the stand-alone app or if you installed the recommended skyKill.py file, type skyKill.py and hit enter. The program will kill skyrim or Fallout 4 and print a confirmation message in the terminal. I should also note that this wont place an icon on your Desktop. It will be in your linux menu. If you want an icon on your desktop you can search for skyKill in your linux menu and right click on it and add it to desktop.```
1. Download the installer from [here][2].

2. Open a terminal and do the following depending on how you want to install skyKill.

```Bash
## I downloaded it to Desktop so cd into desktop.
cd ~/Desktop

## The file needs to be executable.
chmod +x skyKill_installer

## Run the installer without root install. Recommended unless Skyrim is not releasing your 
## mouse and cant slick on the skyKill icon.
./skyKill_installer

## The installer will ask you whether you want to install the python script or if you want
## to install the stand alone application.
## once installed you can find the skyKill app in your Linux menu.

## Root install. 
sudo ./skyKill_installer

## The installer will install skyKill to /usr/local/sbin so you can run from anywhere in 
## the terminal.
## If you installed the python script version open terminal and type
skyKill.py

## or if you installed the stand-alone single file executable type
skyKill

## the program will kill Skyrim or Fallout 4 and print a confirmation message to the 
## terminal. Wont print anything if neither program was running
## Both the root and non root methods will be available in your Linux menu.

## To uninstall just run the installer again. It wont matter which option you 
## choose 1 or 2. If skyKill is installed it will remove it.
## If you installed skyKill as root you will need to be root to remove it. If you
## installed as root and try to remove skyKill while not root it will fail printing
## why it failed and then asking you to rerun as root.

```

[2]: https://github.com/tetoNidan/skyKill/blob/master/InstallerFiles/dist/skyKill_installer



#### Install Manually:
    With a mixture of these steps below and possibly a bit of duckFu, 
    I'm sure you can get this skyKill.py running on any distro.  

1. Download the skyKill.py file from [here][3].
2. Use your File Manager to copy the skyKill.py file to `/usr/local/sbin`. Then skip to # three in the command line instructions below.

[3]: https://github.com/tetoNidan/skyKill/blob/master/skyKill.py
```
cd ~/Where/you/downloaded/the/file
cp skyKill.py /usr/local/sbin/skyKill.py
cd /usr/local/sbin
sudo chown root:root skyKill.py
sudo chmod +x skyKill.py
```

You might have to make the skyKill.py file python 2.7 if you don't have python 3.6+ installed. You can find out what version of python is installed by default by running `python --version` in a terminal.

##### To Convert from python 3.6.7 to 2.7:
LPT: You can do this in reverse and it will have the opposite effect.

1. Remove the number 3 from the shebang.

```Python
#!/usr/bin/env python3

```

    So the shebang looks like this.

```Python
#!/usr/bin/env python

```

2. Remove the outermost parentheses () from the print statements.

```Python
print('This should still be there.')

```

    The prints should look like this.

```Python
print 'This should still be there.'
## or if the print looks like this:
print('{} = {}'.format(x, y))
## you should make it look like this. Just take the first '(' and last ')'
print '{} = {}'.format(x, y)
## you can call your mom and tell her your an internet certified python expert. 
## I should know I've followed a few. ~ John Cleese
```

3. Save the file.

##### Creating The .desktop File:

If you want an icon for it you will have to create a .desktop file. You can create one in:

`/home/$USER/.local/share/applications`

Or whatever is best for your distro. You can use any image you like that your os can read. You can point to any executable file. The .desktop file should look something like this.
```
[Desktop Entry]
Encoding=UTF-8
Version=.5
Type=Application
Exec=/usr/local/sbin/skyKill.py
Name=skyKill
Icon=/home/$USER/.local/share/skyKillrim/data/skyKill.png
``` 
You can name the .desktop file whatever you want as long as it has .desktop as it's extension.

### END:

Thought I'd mention, in case it wasn't painfully obvious. You can go ahead and use my code. I hope it helps. Be free and make something better than this. Also if people start using this app I will probably edit out all the goofy sarcasm in this readme file. As of now Im the only one who uses it and I like the sarcasm. It made learning Git that much better. 
 
