# -*- mode: python -*-

block_cipher = None


a = Analysis(['install_skyKill.py'],
             pathex=['/home/my3al/Dropbox/python/skyKill/InstallerFiles'],
             binaries=[],
             datas=[('data/skyKill.py', 'data'), ('data/skyKill', 'data'), ('data/imperial_96.png', 'data')],
             hiddenimports=[],
             hookspath=[],
             runtime_hooks=[],
             excludes=[],
             win_no_prefer_redirects=False,
             win_private_assemblies=False,
             cipher=block_cipher,
             noarchive=False)
pyz = PYZ(a.pure, a.zipped_data,
             cipher=block_cipher)
exe = EXE(pyz,
          a.scripts,
          a.binaries,
          a.zipfiles,
          a.datas,
          [],
          name='skyKill_installer',
          debug=False,
          bootloader_ignore_signals=False,
          strip=False,
          upx=True,
          runtime_tmpdir=None,
          console=True )
