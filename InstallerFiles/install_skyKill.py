#!/usr/bin/env python3
''' This program installs the skyKill application and icon in their
respective locations. It also creates the skyKill.desktop file.

How to use:
    --> install and removal have the same instructions.

    1: Copy to any location. Desktop, Downloads it doesn't matter.
    2: Open terminal in that location.
    3: Give installer executable privileges.
           root: chmod +x skyKill_installer
           user: chmod u+x skyKill_installer
    4: Install the skyKill.py file, icon, .desktop file and supporting 
       directories.
           root: sudo ./skyKill_installer
           user: ./skyKill_installer
    5: Remove or archive installer for removal.

What exactly does the installer install?

    1: installs the skyKill.py file.
        Locations:
           root: /usr/local/sbin/skyKill.py
           user: /home/$USER/.local/share/skyKillrim/skyKill.py

    2: Installs skyKill.png image file for icon. Same for $USER and root.
           Location:  /home/$USER/.local/share/skyKillrim/data/skyKill.png

    3: Creates a skyKill.desktop file based on executable location. 
       Same for $USER and root.
           Location: /home/$USER/.local/share/applications/skyKill.desktop

The uninstaller will remove all files and their directories that this file
had created. This file is the installer and uninstaller. If skyKill installed
will uninstall. If skyKill is uninstalled will install.
'''

## Below this point are some notes I left in in case I want to make a tutorial of some kind. They are pyinstaller
## compile notes if you were wondering.

## Here is the command I used to generate my spec file. 

## Linux:
## pyinstaller -F -n skyKill_installer --add-data data/skyKill.py:data --add-data data/imperial_96.png:data install_skyKill.py

## Windows:
## pyinstaller -F -n skyKill_installer --add-data data/skyKill:data --add-data data/imperial_96.png:data install_skyKill.py

## Note the --add-data file:destination (The destination relative to _MEIPASS or what folder the file can be found in if _MEIPASS is /)
## in other words template the file structure pyinstaller will have create at compile so when the program is running so it knows where to look
## for your files.

import sys
import importlib
from os import makedirs as oMakedirs, mkdir as oMkdir, rmdir as oRmdir, remove as oRemove
from os.path import abspath as oAbspth, expanduser as oExpandusr, exists as oExists, join as oJoin, split as oSplit, dirname as oDirname, isdir as oIsdir
from getpass import getuser
import subprocess as subP
from collections import OrderedDict as OD
from shutil import copy2, rmtree
from re import split as rSplit
from textwrap import TextWrapper

try:
    import pip
    import apt
except ImportError as e:
    print(e)

class make_install():

    def __init__(self, *args, **kargs):

        ## Create Application runtime dependent Location Variables
        if getattr(sys, 'frozen', False):
            ## We are running in a |PyInstaller|
            dataDir = oJoin(sys._MEIPASS, 'data')
        else:
            ## We are running in a normal Python environment
            dataDir = oJoin(oAbspth(oDirname(__file__)), 'data')

        ## We are about to test if we have root permissions. 
        ## We really might need to know this again later if we need to set -rwx:r-x:r-x 
        ## for command line use subprocesses?
        self.isRoot = False
        self.xOut = TextWrapper(tabsize=3, subsequent_indent='  ', replace_whitespace=False)

        ## set the baseDir
        baseDir = oExpandusr('~')
        ## set the location for the share variable relative to the
        ## baseDir.
        shareLoc = oJoin(baseDir, '.local', 'share')
        mkFileDirs(shareLoc)
        ## Now that we know where to make our file system in the users
        ## environment. Set that shit up man.
        skyKillLoc = oJoin(shareLoc, 'skyKillrim')
        mkFileDirs(oJoin(skyKillLoc, 'data'))
        ## set the location for the icon variable.
        iconLoc = oJoin(skyKillLoc, 'data', 'skyKill.png')
        ## set the location for the skyKill.desktop variable location.
        dsktpLoc = oJoin(oExpandusr('~'), '.local', 'share', 'applications')
        ## make the ,desktop file location tree
        mkFileDirs(dsktpLoc)
        ## set the location for the skyKill.desktop variable
        dsktpLoc = oJoin(dsktpLoc, 'skyKill.desktop')

        ## Test for root access and set base directory's accordingly.
        try:
            ## To make a directory in / to see if we have root. Name it something
            ## that no rational programmer would ever do so as to:
            # 1: Avoid file name conflicts
            # 2: Because I can.
            oMkdir('/etc/superFunkTasticSubParBand_HarryAndTheHendersonsTakenRideInAvan')
        except IOError as e:
            if '[Errno 13]' in str(e):
                ### Oh noooo. No root so make the base directory the users home directory
                ## aka $HOME.
                
                ## Ask the user how they want the install to proceed. Choose script or
                ## single file executable.
                instalType = self.which_install(args)
                ## User has decided. Apply their choice in files for install.
                instalFile = ['skyKill.py', 'skyKill'][instalType - 1]
                
                ## set the location for the skyKill application variable.
                appLoc = oJoin(skyKillLoc, instalFile)
        else:
            ## LPT if you read the directory that we created earlier with the
            ## rhythm of the doobeeBros it becomes easier to type. Any way we don't need
            ## that anymore so, get rid ov it mon.
            oRmdir('/etc/superFunkTasticSubParBand_HarryAndTheHendersonsTakenRideInAvan')
            rootDir = oExpandusr('/')
            ## set the location for the share variable relative to the
            ## baseDir.
            #
            ### we are root so set isRoot to True for later.
            self.isRoot = True

            ## set the location for the skyKill application variable.
            appLoc = oJoin(rootDir, 'usr', 'local', 'sbin')
            mkFileDirs(appLoc)

            ## Ask the user how they want the install to proceed. Choose script or
            ## single file executable.
            print(args)
            instalType = self.which_install(args)        
            ## User has decided. Apply their choice in files for install.
            instalFile = ['skyKill.py', 'skyKill'][instalType - 1]

            appLoc = oJoin(appLoc, instalFile)

        ## Use Application runtime dependent variables to set our app and image.
        ## These are the files that will be moved.

        ## The application skyKill itself.
        sk_app = oJoin(dataDir, instalFile)
        ## The Icon we are going to use
        sk_img = oJoin(dataDir, 'imperial_96.png')
        ## The string that we will write to skyKill.desktop
        ## after we have set the proper locations. 
        sk_desktop = ''

        ## Prototype for .desktop file comes from 
        ## https://linuxcritic.wordpress.com/2010/04/07/anatomy-of-a-desktop-file/
        desktop = OD()
        desktop['Header'] = '[Desktop Entry]'
        desktop['Encoding'] = 'UTF-8'
        desktop['Version'] = '.75'
        desktop['Type'] = 'Application'
        desktop['Exec'] = '$HOME/MyApp'
        desktop['Name'] = 'skyKill'
        desktop['Icon'] = '$HOME/Icons/MyIcon.png'


        ## This seems like a good place to place the uninstaller.
        ## The program will either install or remove the program and files.
        if oExists(dsktpLoc):
            ## Ok, so the user hates us and told us to take our ball and go. Lets be a good guy and
            ## look for ALL THE FILES and remove them. Luckily that isn't so hard as a record of the
            ## install paths are in the skyKill.desktop file. 

            ## we will read and parse the skyKill.desktop file to get the file installed locations.
            dsktpData = {}
            with open(dsktpLoc) as iFile:
                dsktpData = {line.split('=')[0]: line.split('=')[1].strip() for line in iFile if '=' in line and oExists(line.split('=')[1].strip())}

            ## loop and test.
            for key, rmData in dsktpData.items():
                ## If system has root install but not root access. this is a catch and kill. No else needed.
                if not self.isRoot and not getuser() in rmData:
                    ## The not isRoot is self explanatory. 
                    ## The getuser() returns the $USER and checks if the user is in the file path to the
                    ## executable file. If it is not there you are root install location.
                    sys.exit("You have installed this program using root access and as such you \ndon't have the proper file permissions to remove it. Please rerun as root.")

                ## the user has root access and can remove any file or the user doesn't need root to remove 
                ## them.
                if not oIsdir(rmData):
                    oRemove(rmData)
                print('{}: Removed from {}'.format(key, rmData))

            ## Now that the install dir is clean we can remove the dir tree with os. 
            # print(dsktpData['Exec'])
            rmtree(skyKillLoc)
            ## remove the skyKill.desktop file.
            oRemove(dsktpLoc)
            print("The program has been removed. Thanks for trying it out.")

        else:
            ## user said he wants to give this puppy a whirl. Whelp, let's oblige'm.
            ## Edit the desktop template to reflect where we will
            ## put the app and icon.
            desktop['Exec'] = appLoc
            desktop['Icon'] = iconLoc

            ## create the skyKill.desktop file. Follow the dictionary Lebowski
            for key, val in desktop.items():
                if key == 'Header':
                    sk_desktop = val
                else:
                    sk_desktop = '{}\n{}={}'.format(sk_desktop, key, val)

            ## move the skyKill app to its nesting grounds.
            copy2(sk_app, appLoc)

            ## move the image to its new home.
            copy2(sk_img, iconLoc)
            ## write the skyKill.desktop file so it can hopefully direct the user 
            ## to application icon and all. Fingers crossed yall.
            dtFile = open(dsktpLoc, 'w+')
            dtFile.write(sk_desktop)
            dtFile.close() 

            if self.isRoot:
                ## So we are root. We need the users $USER folder. We'll just split the 
                ## expand user variable and use its second value.
                userName = oSplit(oExpandusr('~'))[1]
                ## Root install we want to make sure:
                ##   chmod: files have the correct permissions to run as root
                ##   chown: files are owned by the $USER. If they were owned by
                ##          root user the $USER could not edit easily. Since they
                ##          are in the $USER's folder user needs access.
                cmds = [['sudo', 'chmod', '+x', '{}'.format(appLoc)],
                        ['sudo', 'chown', '-R', '{0}:{0}'.format(userName), '{}'.format(appLoc)],
                        ['sudo', 'chown', '-R', '{0}:{0}'.format(userName), '{}'.format(skyKillLoc)],
                        ['sudo', 'chown', '-R', '{0}:{0}'.format(userName), '{}'.format(oSplit(dsktpLoc)[0])]]

                for cmd in cmds:
                    ## Loop and run commands.
                    ps = subP.Popen(cmd, stdout=subP.PIPE)
                    ## here we could do some error handling and print the stdout of the process.
                    ## If I did everything right there should never be output worth printing.
            else:
                ## No root. We still need to make the application executable.
                ## chmod: only give $USER executable permissions. No group
                cmds = [['chmod', 'u+x', '{}'.format(appLoc)]]
                for cmd in cmds:
                    ## Loop and run commands.
                    ps = subP.Popen(cmd, stdout=subP.PIPE)
                    ## here we could do some error handling and print the stdout of the process.
                    ## If I did everything right there should never be output worth printing.

            ## print install confirmation message!
            print("Hail Sithis!")

    def termWrap(self, txt):
        ''' Simple function to format text wrapping for use in the terminal with large strings.
        For many calls consider moving the xOut to be of super global level as the textWrapper
        object will only be called once. '''
        #xOut = TextWrapper(tabsize=3, subsequent_indent='  ', replace_whitespace=False)
        return '\n'.join([self.xOut.fill(x) for x in txt.splitlines()])


    def which_install(self, *args):
        ''' Function asks for the users preference of install types.
            1: Python script skyKill.py
            2: Single file executable. '''
        info = '''\n  ==============  INFO ==============
    There are three ways to install the skyKill application. I will explain them in detail. Both are the exactly the same script, they only differ in how they are executed.

1: Install as python script. This is the preferred method. It is smaller in file size. Uses your native python3. Should be installed by default. This requires one PIP modual to run. PSUTIL, further details found in ( d: ). 

   What it installs?

    a: installs the skyKill.py file. One or the other. Run as root to make root install. 
        Locations:
           root: /usr/local/sbin/skyKill.py
           user: /home/$USER/.local/share/skyKillrim/skyKill.py

    b: Installs skyKill.png image file for icon. Same for $USER and root.
        Location:  
            /home/$USER/.local/share/skyKillrim/data/skyKill.png

    c: Creates a skyKill.desktop file based on executable location. Desktop file links the program with the icon. 
       Same for $USER and root.
        Location: 
            /home/$USER/.local/share/applications/skyKill.desktop

    d: Needs to install python modual PSUTIL and everything it takes to run psutil. Psutil is a library that helps in operating system operations. I only use one class from pricess_iter. It handles the process iteration and killing.

       This program needs and will attempt to install 
          1: apt-get => ( python3-dev and python3-pip )
          2: pip => ( setuptools and psutil )

    Extra cool tip. You can edit and improve the skyKill.py file if you ever want to.


2: Installs the single file executable. This method is foolproof. No need to check for python version compatibility as it contains everything the program needs to run. The file size is quite large compared to option 1. Think of it as a second python installation you can only use for this one single purpose. SkyKill is just a simple script and isn't really a good use of the single file executable. I don't want to tell what to do but I prefer the first option. 
       Will not work on Debian Version's less than Stretch or
       any versions of linux versions lower than Stretch.
       Strech is equal to Ubuntu:Xenial and Mint:Sarah

   a: Attributes from Option 1 a-c are the exact same.

'''

        ask = '''Type Exit to exit installer at any time.
    Type Info to see information about this program.
        Choose installer Option 1 or 2?'''

        print(self.termWrap(info))
        while True:
            print(ask)
            try:
                ## Try for user raw input.
                ans = input('1 or 2 > ')
            except KeyboardInterrupt:
                ## Except keyboard interrupt and exit without error.
                sys.exit('\nGoodbye!\n')
            else:
                if ans in ['Exit', 'exit', 'Quit', 'quit']:
                    ## User has decided to quit
                    sys.exit('Goodbye!')
                elif ans in ['info', 'Info', 'about', 'About', 'help', 'Help']:
                    ## user wants to see the info for the program
                    print(self.termWrap(info))
                elif ans in [1, '1']:
                    ## user wants to install the script
                    if self.isRoot:
                        ## we need to know that we have pip, ptyhon-dev and setuptools.
                        ## Install them if we are root.

                        ## Run a quick test to see if everything has been setup already
                        ## avoids calling apt-get
                        qTest = install_and_import('psutil')
                        if qTest != True:
                            ## the user needs the python-dev and python-pip installed.
                            pyNeeds()

                        ## try to get setuptools
                        sTools = install_and_import('setuptools')

                        if sTools:
                             ## try to get psutil
                            insPsutil = install_and_import('psutil')
                            if insPsutil:
                                return 1
                            else:
                                sys.exit('{}'.format(insPsutil))
                        else:
                            sys.exit('{}'.format(sTools))
                    else:
                        return 1

                elif ans in [2, '2']:
                    ## user wants to install the program.
                    return 2


## helper function to make directory trees if they don't exist.
def mkFileDirs(qDir):
    ''' This function makes the directory that aren't there. '''
    if not oExists(qDir):
        oMakedirs(qDir)


def pyNeeds():
    ''' So after further testing I remembered that the psutil package might
    need the python3-dev and pip itself. If they are not installed. '''
    cmd = ['sudo', 'apt-get', 'install', '-y', 'python3-dev', 'python3-pip']

    proCom = procRead(cmd)

    if proCom[1] != '':
        print(proCom[1])
    else:
        vChkStr = ' is already the newest version'
        newest = [x.replace(vChkStr, '') for x in proCom[0].split('\n') if vChkStr in x]

        if newest != []:
            t = 0
            rP = vChkStr.strip().replace('is', 'The following are') + '.'
            for nw in newest:
                t += 1
                rP = str(rP + '\n' + nw).strip()
            if t < 2:
                print(rP)
                print(proCom[0])
            #else:
                ## all python packages have already been satisfied.
                ## we could print something but the whole purpose of checking
                ## was so I could ignore it and just print Hail Sithis.
        else:
            print(proCom[0])


def aptInstall(package):
    cache = apt.cache.Cache()
    cache.update()
    cache.open()

    pkg = cache[package]
    if pkg.is_installed:
        print('{} already installed'.format(package))
    else:
        pkg.mark_install()
        try:
            cache.commit()
        except Exception as e:
            print(e)


def procRead(cmd):
    process = subP.Popen(cmd, bufsize=1, universal_newlines=True, stdout=subP.PIPE, stderr=subP.STDOUT)
    outPut = ''
    for line in iter(process.stdout.readline, ''):
        if line != '':
            outPut = '{}\n{}'.format(outPut, line)
            print(line.strip())
        sys.stdout.flush() # please see comments regarding the necessity of this line 
    process.wait()
    errcode = process.returncode
    # print(errcode)
    return (outPut, '') if errcode == 0 else (outPut, errcode)


def install_and_import(package):
    ''' Function does its best to make sure you have the package you need. '''
    try:
        importlib.import_module(package)
        # print('{} has already been installed.'.format(package))
        return True
    except ImportError:
        cmd = ['pip3', 'install', package]
        try:
            ps = subP.Popen(cmd, stdout=subP.PIPE, stderr=subP.PIPE)
            proCom = list(ps.communicate())[0]
            if 'Requirement already satisfied' not in proCom.decode():
                sys.stdout.write(proCom.decode())
            return True
        except Exception as e:
            # print(e)
            return False
            
    else:
        # print('{} can not be installed.'.format(package))
        return '{} can not be installed.'.format(package)


def mkArgs(dlArgs):
    if len(dlArgs) > 1:
        lt, dt = [], {}
        for ar in dlArgs[1:]:
            if '=' in ar:
                a = ar.split('=')
                if a[1] in ['false', 'False']:
                    dt[a[0]] = False
                elif a[1] in ['true', 'True']:
                    dt[a[0]] = True
                else:
                    dt[a[0]] = a[1]
            else:
                lt.append(ar)
        return (lt, dt)
    else:
        return None

## ask if the program is executing. If so run function.
if __name__ == '__main__':
    args = mkArgs(sys.argv)
    if args is None:
        make_install()
    # elif args[0] == [] and args[1] != {}:
    #     make_install(**args[1])
    elif args[0] != [] and args[1] == {}:
        make_install(*args[0])
    # else:
    #     make_install(*args[0], **args[1])
