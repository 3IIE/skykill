#!/usr/bin/env python3
#################################
###     _          _  ___ _ _ ###
### ___| | ___   _| |/ (_) | |###
###/ __| |/ / | | | ' /| | | |###
###\__ \   <| |_| | . \| | | |###
###|___/_|\_\\__, |_|\_\_|_|_|###
###          |___/            ###
#################################

''' skyKill is a program to combat skyrim's unwillingness to fucking die
when told. So instead of scooting around the desktop ui or if you fancy
yourself a keyboard cowboy you can just stop wasting your time. 
As simple as coc BoringTownReality. '''

## import the splitext class from os.path.
## On linux there arent many processes that end in a .exe as
## that is a windows executable. Steam proton is an emulation
## layer that runs .exe's. We will exploit that to decide which 
## programs to kill.
from os.path import splitext
from os import name as oName

## import the process_iter class from psutil.
## This class will handle the processes.
from psutil import process_iter as psit

def killSkyrim():
    ''' Simple function that kills every running process that has a 
     .ext extension. '''

    ## Check if we are running in a Windows or *nix like OS.
    if oName == 'nt':
        ## Hi Windows. I'll test you out sooner or later I'm sure. It all works.
        ## Tested and compiled. I lives in it's own branch(windowsOS) for now.
        [x.kill() for x in psit() if x.name() == 'TESV.exe']
        print('Dont look now but Linux just stole your sweet role!')
    else:
        ## Likely we are in a posx. Good enough for me.
        ## Alright, Those Kirk lovin Spock suckers are gone. In a freedesktop is the 
        ## plaice for me. Never in windows do I want to be.
        
        ## Get all the wine apps.
        RunningProc = [x.name() for x in psit() if splitext(x.name())[1] == '.exe']
        try:
            game = [x for x in RunningProc if x in ['TESV.exe', 'Fallout4.exe']][0]
        except IndexError:
            game = None

        print(game)
        
        if game in RunningProc:

            ## Wine services that Skyrim starts list.
            SkyList = ['services.exe', 'winedevice.exe', 'plugplay.exe', 'winedevice.exe', 'explorer.exe', game]

            ## Check if other .exe's is running in your processes. We will then ignore 
            ## those other processes. [Photoshop.exe, Vortex.exe, and all their necessities]
            if len(RunningProc) > len(SkyList):
                ## Make new list of processes to kill. 
                SkyList = [x for x in SkyList if x == game]

            ## Here I use the kill() method on the process if .exe is at the end of
            ## name() method. All done in a list comprehension loop.
            [x.kill() for x in psit() if x.name() in SkyList]
            print('I used to be a adventurer like you but then I took an arrow in the knee.')

if __name__ == '__main__':
    ## Run the program
    killSkyrim()
